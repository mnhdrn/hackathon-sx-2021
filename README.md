# STARTX HACKATHON 2021

## Installation

Provision a CentOs 8 machine with the following ressource :
- CPU = 
- RAM = 
- STOCKAGE = 

```
yum install -y git
git clone https://gitlab.com/mnhdrn/hackathon-sx-2021 /root/hackathon-sx-2021
```

enter the directory and launch the **setup.sh**

You may want to add the following flag at the execution of playbook config-awx.yml

this will let the hackathon be another machine to avoid user to exec on aws container.

```
-e node_ip='your.ip.address.here'
```

--- 

## AWX

#### Add-user

This playbook will create a player on the machine.

You need to give username, password and the admin password of awx for this playbook to run.

This playbook will also create a job template name Check-user for each created user.


#### Check-user

This job template will run every minute for 24 hours.

when a user connect with SSH a file is created in /tmp.

while the file in /tmp is absent the Job template end as an error.

this script will check for two type of file in /tmp :
- user_login.time
- user_login.end

when present on the /tmp those file will be moved to a secure place on /root/user_times

then the job template will compare the current time with the start time stock on /root/user_times/user_login.time

if it exceed a given time (by default 20minutes) it will launch the closing process for a delegates.

if a file user_login.end is present it will also launch the closing process for the delegate.

the closing process will:
- kill the user session
- change user password to redhat
- stop the scheduling process of the job template
- launch grading job template
