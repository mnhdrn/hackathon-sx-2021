#!/bin/sh

echo " --------- Installing prequisite "
yum install -y epel-release
yum install -y ansible

echo " --------- Preparing AWX Installation"
ansible-playbook -i inventory install-awx.yml

sleep 3;

echo " --------- INSTALLING AWX"
ansible-playbook \
	-e postgres_data_dir="/var/lib/pgsql" \
	-e project_data_dir="/var/lib/awx/projects" \
	-e admin_password="secretdemerde" \
	-i /root/awx/installer/inventory \
	/root/awx/installer/install.yml

echo " --------- CONFIG AWX"
ansible-playbook \
	-e admin_password="secretdemerde" \
	-e root_password="redhat" \
#	-e node_ip=''
	config-awx.yml
