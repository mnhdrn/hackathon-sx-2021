#!/bin/sh

for i in Debian Ubuntu Kali Parrot Redhat Fedora Centos Oracle;
do
	neofetch --ascii_distro $i -L > ascii-$i.txt
done
